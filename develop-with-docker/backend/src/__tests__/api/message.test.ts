import app from '../../index'
import * as supertest from 'supertest'
import { Message } from '../../entity/message'
import { getRepository, getConnection } from '../../db/connection'
const req = supertest.agent(app)

beforeAll(async () => {
  const connection = await getConnection()
  await connection
    .createQueryBuilder()
    .delete()
    .from(Message)
    .execute()
})

describe('message test', () => {
  test('call success', async () => {
    await req
      .get('/api/v1/messages')
      .set('Accept', 'application/json')
      .expect(async res => {
        console.log('res.status:' + res.status)
        expect(res.status).toBe(200)
        console.log(res.body)
      })
      .catch(err => {
        throw err
      })

    const message: Message = {
      id: 0,
      message: 'hogefuga',
    }
    await req
      .post('/api/v1/messages')
      .set('Accept', 'application/json')
      .send(message)
      .expect(async res => {
        console.log('res.status:' + res.status)
        expect(res.status).toBe(200)
        console.log(res.body)
      })
      .catch(err => {
        throw err
      })

    await req
      .get('/api/v1/messages')
      .set('Accept', 'application/json')
      .expect(async res => {
        expect(res.status).toBe(200)
        console.log(res.body)
        const results: Message[] = res.body[0]
        const count = res.body[1]

        expect(count).toBe(1)
        expect(results.length).toBe(1)
        expect(results[0].message).toBe('hogefuga')
      })
      .catch(err => {
        throw err
      })
  })
})
